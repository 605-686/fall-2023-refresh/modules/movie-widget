package com.androidbyexample.movie.repository

interface HasId {
    val id: String
}
