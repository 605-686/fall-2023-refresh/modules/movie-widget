package com.androidbyexample.movie.repository

import android.content.Context
import com.androidbyexample.movie.data.MovieDao
import com.androidbyexample.movie.data.createDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class MovieDatabaseRepository(
    private val dao: MovieDao
): MovieRepository {
    override val ratingsFlow =
        dao.getRatingsFlow()
            .map { ratings ->// for each List<RatingEntity> that's emitted
                // create a list of RatingDto
                ratings.map { rating -> rating.toDto() } // map each entity to Dto
            }
    override val moviesFlow =
        dao.getMoviesFlow()
            .map { movies ->
                movies.map { it.toDto() }
            }
    override val actorsFlow =
        dao.getActorsFlow()
            .map { actors ->
                actors.map { it.toDto() }
            }

    override fun getRatingWithMoviesFlow(id: String): Flow<RatingWithMoviesDto> =
        dao.getRatingWithMoviesFlow(id)
            .map { it.toDto() }

    override suspend fun getMovie(id: String): MovieDto =
        dao.getMovie(id).toDto()
    override suspend fun getRatingWithMovies(id: String): RatingWithMoviesDto =
        dao.getRatingWithMovies(id).toDto()

    override suspend fun getMovieWithCast(id: String): MovieWithCastDto =
        dao.getMovieWithCast(id).toDto()

    override suspend fun getActorWithFilmography(id: String): ActorWithFilmographyDto =
        dao.getActorWithFilmography(id).toDto()


    override suspend fun upsert(movie: MovieDto) = dao.upsert(movie.toEntity())
    override suspend fun upsert(actor: ActorDto) = dao.upsert(actor.toEntity())
    override suspend fun upsert(rating: RatingDto) = dao.upsert(rating.toEntity())

    override suspend fun insert(movie: MovieDto) = dao.insert(movie.toEntity())
    override suspend fun insert(actor: ActorDto) = dao.insert(actor.toEntity())
    override suspend fun insert(rating: RatingDto) = dao.insert(rating.toEntity())

    override suspend fun deleteMoviesById(ids: Set<String>) = dao.deleteMoviesById(ids)
    override suspend fun deleteActorsById(ids: Set<String>) = dao.deleteActorsById(ids)
    override suspend fun deleteRatingsById(ids: Set<String>) = dao.deleteRatingsById(ids)

    override suspend fun resetDatabase() = dao.resetDatabase()

    companion object {
        fun create(context: Context) =
            MovieDatabaseRepository(createDao(context))
    }
}
