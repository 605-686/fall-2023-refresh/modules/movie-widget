package com.androidbyexample.movie

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.androidbyexample.movie.screens.MovieDisplay
import com.androidbyexample.movie.screens.MovieList
import com.androidbyexample.movie.screens.Ui
import com.androidbyexample.movie.ui.theme.MovieUi1Theme

// ##START 030-key-name
const val MOVIE_ID_EXTRA = "movieId"
// ##END

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel> { MovieViewModel.Factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // ##START 030-on-create
        intent?.handleMovieId()
        // ##END
        setContent {
            MovieUi1Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Ui(viewModel) {
                        finish()
                    }
                }
            }
        }
    }

    // ##START 030-on-new-intent
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.handleMovieId()
    }
    // ##END

    // ##START 030-handle-movie-id
    private fun Intent.handleMovieId() {
        val movieId = extras?.getString(MOVIE_ID_EXTRA)
        if (movieId != null) {
            viewModel.setScreens(MovieList, MovieDisplay(movieId))
        }
    }
    // ##END
}
