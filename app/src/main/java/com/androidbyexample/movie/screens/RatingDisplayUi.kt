package com.androidbyexample.movie.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movie.R
import com.androidbyexample.movie.components.Display
import com.androidbyexample.movie.components.Label
import com.androidbyexample.movie.repository.MovieDto
import com.androidbyexample.movie.repository.RatingWithMoviesDto

@Composable
fun RatingDisplayUi(
    ratingWithMovies: RatingWithMoviesDto?,
    onMovieClicked: (MovieDto) -> Unit,

    selectedIds: Set<String>,
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedMovies: () -> Unit,

    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
) {
    ListScaffold(
        titleId = R.string.rating,
        items = ratingWithMovies?.movies?.sortedBy { it.title } ?: emptyList(),
        onItemClicked = onMovieClicked,
        selectedIds = selectedIds,
        onSelectionToggle = onSelectionToggle,
        onClearSelections = onClearSelections,
        onDeleteSelectedItems = onDeleteSelectedMovies,
        currentScreen = currentScreen,
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase,
        itemContent = { movie ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(8.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Movie,
                    contentDescription = stringResource(id = R.string.movie),
                    modifier = Modifier.clickable {
                        onSelectionToggle(movie.id)
                    }
                )
                Display(text = movie.title)
            }
        },
        topContent = {
            ratingWithMovies?.let { ratingWithMovies ->
                Label(textId = R.string.name)
                Display(text = ratingWithMovies.rating.name)
                Label(textId = R.string.description)
                Display(text = ratingWithMovies.rating.description)
                Label(
                    text = stringResource(
                        id = R.string.movies_rated,
                        ratingWithMovies.rating.name
                    )
                )
            }
        }
    )
}
