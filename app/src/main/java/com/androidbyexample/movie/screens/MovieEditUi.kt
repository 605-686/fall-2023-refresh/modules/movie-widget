package com.androidbyexample.movie.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movie.R
import com.androidbyexample.movie.repository.MovieDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieEditUi(
    id: String,
    fetchMovie: suspend (String) -> MovieDto,
    onMovieChange: (MovieDto) -> Unit,
) {
    var movie by remember { mutableStateOf<MovieDto?>(null) }
    LaunchedEffect(key1 = id) {
        withContext(Dispatchers.IO) {
            movie = fetchMovie(id)
        }
    }
    Scaffold(
        topBar = {
            TopAppBar(title = { Text(text = movie?.title ?: "") })
        }
    ) { paddingValues ->
        Column(
            modifier = Modifier.padding(paddingValues)
        ) {
            movie?.let { fetchedMovie ->
                OutlinedTextField(
                    value = fetchedMovie.title,
                    label = { Text(text = stringResource(id = R.string.title))},
                    onValueChange = {
                        val newMovie = fetchedMovie.copy(title = it)
                        movie = newMovie
                        onMovieChange(newMovie)
                    },
                )
                OutlinedTextField(
                    value = fetchedMovie.description,
                    label = { Text(text = stringResource(id = R.string.description))},
                    onValueChange = {
                        val newMovie = fetchedMovie.copy(description = it)
                        movie = newMovie
                        onMovieChange(newMovie)
                    },
                )
            }
        }
    }
}
