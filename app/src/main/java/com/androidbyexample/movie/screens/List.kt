package com.androidbyexample.movie.screens

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.androidbyexample.movie.repository.HasId

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun <T: HasId> List(
    state: LazyListState,
    items: List<T>,
    onItemClicked: (T) -> Unit,

    selectedIds: Set<String>,
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,

    modifier: Modifier = Modifier,
    topContent: (@Composable () -> Unit)? = null,
    itemContent: @Composable ColumnScope.(T) -> Unit,
) {
    LazyColumn(
        state = state,
        modifier = modifier
    ) {
        topContent?.let {
            item {
                topContent()
            }
        }

        items(
            items = items,
            key = { it.id },
        ) { item ->
            val containerColor =
                if (item.id in selectedIds) {
                    MaterialTheme.colorScheme.secondary
                } else {
                    MaterialTheme.colorScheme.surface
                }
            val contentColor = MaterialTheme.colorScheme.contentColorFor(containerColor)

            if (selectedIds.isNotEmpty()) {
                BackHandler {
                    onClearSelections()
                }
            }

            Card(
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 8.dp,
                ),
                colors = CardDefaults.cardColors(
                    containerColor = containerColor,
                    contentColor = contentColor,
                ),
                modifier = Modifier
                    .padding(8.dp)
                    .combinedClickable(
                        onClick = {
                            if (selectedIds.isEmpty()) {
                                onItemClicked(item)
                            } else {
                                onSelectionToggle(item.id)
                            }
                        },
                        onLongClick = {
                            onSelectionToggle(item.id)
                        },
                    )
            ) {
                itemContent(item)
            }
        }
    }
}
