package com.androidbyexample.movie.screens

import androidx.annotation.StringRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movie.R
import com.androidbyexample.movie.components.ScreenSelectButton
import com.androidbyexample.movie.repository.HasId

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun <T: HasId> ListScaffold(
    @StringRes titleId: Int,
    items: List<T>,
    onItemClicked: (T) -> Unit,

    selectedIds: Set<String>,
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedItems: () -> Unit,

    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,

    onResetDatabase: () -> Unit,

    onEdit: (() -> Unit)? = null,

    topContent: (@Composable () -> Unit)? = null,
    itemContent: @Composable ColumnScope.(T) -> Unit,
) {
    val lazyListState = remember { LazyListState() }
    val showBottomBar by remember(selectedIds) {
        derivedStateOf {
            lazyListState.firstVisibleItemScrollOffset == 0 && selectedIds.isEmpty()
        }
    }

    Scaffold(
        topBar = {
            if (selectedIds.isEmpty()) {
                TopAppBar(
                    title = { Text(text = stringResource(titleId)) },
                    actions = {
                        onEdit?.let {
                            IconButton(onClick = onEdit) {
                                Icon(
                                    imageVector = Icons.Default.Edit,
                                    contentDescription = stringResource(R.string.edit)
                                )
                            }
                        }
                        IconButton(onClick = onResetDatabase) {
                            Icon(
                                imageVector = Icons.Default.Refresh,
                                contentDescription = stringResource(R.string.reset_database)
                            )
                        }
                    }
                )
            } else {
                TopAppBar(
                    navigationIcon = {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = stringResource(R.string.clear_selections),
                            modifier = Modifier.clickable(onClick = onClearSelections),
                        )
                    },
                    title = { Text(text = selectedIds.size.toString(), modifier = Modifier.padding(8.dp)) },
                    actions = {
                        IconButton(onClick = onDeleteSelectedItems) {
                            Icon(
                                imageVector = Icons.Default.Delete,
                                contentDescription = stringResource(R.string.delete_selected_items)
                            )
                        }
                    },
                )
            }
        },
        bottomBar = {
            AnimatedVisibility(
                visible = showBottomBar,
                enter = expandVertically() + fadeIn(),
                exit = shrinkVertically() + fadeOut(),
            ) {
                NavigationBar {
                    ScreenSelectButton(
                        targetScreen = RatingList,
                        imageVector = Icons.Default.Star,
                        labelId = R.string.ratings,
                        currentScreen = currentScreen,
                        onSelectListScreen = onSelectListScreen
                    )
                    ScreenSelectButton(
                        targetScreen = MovieList,
                        imageVector = Icons.Default.Movie,
                        labelId = R.string.movies,
                        currentScreen = currentScreen,
                        onSelectListScreen = onSelectListScreen
                    )
                    ScreenSelectButton(
                        targetScreen = ActorList,
                        imageVector = Icons.Default.Person,
                        labelId = R.string.actors,
                        currentScreen = currentScreen,
                        onSelectListScreen = onSelectListScreen
                    )
                }
            }
        },
        modifier = Modifier.fillMaxSize()
    ) { paddingValues ->
        List(
            state = lazyListState,
            items = items,
            onItemClicked = onItemClicked,
            selectedIds = selectedIds,
            onSelectionToggle = onSelectionToggle,
            onClearSelections = onClearSelections,
            topContent = topContent,
            modifier = Modifier
                .padding(paddingValues)
                .fillMaxSize(),
            itemContent = itemContent,
        )
    }
}