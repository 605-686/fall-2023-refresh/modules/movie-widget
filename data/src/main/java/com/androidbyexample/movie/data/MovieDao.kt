package com.androidbyexample.movie.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Upsert
import kotlinx.coroutines.flow.Flow

@Dao
abstract class MovieDao {
    @Query("SELECT * FROM RatingEntity")
    abstract fun getRatingsFlow(): Flow<List<RatingEntity>>

    @Query("SELECT * FROM MovieEntity")
    abstract fun getMoviesFlow(): Flow<List<MovieEntity>>

    @Query("SELECT * FROM ActorEntity")
    abstract fun getActorsFlow(): Flow<List<ActorEntity>>

    @Transaction
    @Query("SELECT * FROM RatingEntity WHERE id = :id")
    abstract suspend fun getRatingWithMovies(id: String): RatingWithMovies

    @Transaction
    @Query("SELECT * FROM RatingEntity WHERE id = :id")
    abstract fun getRatingWithMoviesFlow(id: String): Flow<RatingWithMovies>
    @Query("SELECT * FROM MovieEntity WHERE id = :id")
    abstract suspend fun getMovie(id: String): MovieEntity

    @Transaction
    @Query("SELECT * FROM ActorEntity WHERE id = :id")
    abstract suspend fun getActorWithFilmography(id: String): ActorWithFilmography

    @Transaction
    @Query("SELECT * FROM MovieEntity WHERE id = :id")
    abstract suspend fun getMovieWithCast(id: String): MovieWithCast

    @Insert
    abstract suspend fun insert(vararg ratings: RatingEntity)
    @Insert
    abstract suspend fun insert(vararg movies: MovieEntity)
    @Insert
    abstract suspend fun insert(vararg actors: ActorEntity)
    @Insert
    abstract suspend fun insert(vararg roles: RoleEntity)

    @Upsert
    abstract suspend fun upsert(vararg ratings: RatingEntity)
    @Upsert
    abstract suspend fun upsert(vararg movies: MovieEntity)
    @Upsert
    abstract suspend fun upsert(vararg actors: ActorEntity)
    @Upsert
    abstract suspend fun upsert(vararg roles: RoleEntity)

    @Query("DELETE FROM MovieEntity WHERE id IN (:ids)")
    abstract suspend fun deleteMoviesById(ids: Set<String>)
    @Query("DELETE FROM ActorEntity WHERE id IN (:ids)")
    abstract suspend fun deleteActorsById(ids: Set<String>)
    @Query("DELETE FROM RatingEntity WHERE id IN (:ids)")
    abstract suspend fun deleteRatingsById(ids: Set<String>)

    @Query("DELETE FROM MovieEntity")
    abstract suspend fun clearMovies()
    @Query("DELETE FROM ActorEntity")
    abstract suspend fun clearActors()
    @Query("DELETE FROM RatingEntity")
    abstract suspend fun clearRatings()
    @Query("DELETE FROM RoleEntity")
    abstract suspend fun clearRoles()

    @Transaction
    open suspend fun resetDatabase() {
        clearMovies()
        clearActors()
        clearRoles()
        clearRatings()

        insert(
            RatingEntity(id = "r0", name = "Not Rated", description = "Not yet rated"),
            RatingEntity(id = "r1", name = "G", description = "General Audiences"),
            RatingEntity(id = "r2", name = "PG", description = "Parental Guidance Suggested"),
            RatingEntity(id = "r3", name = "PG-13", description = "Unsuitable for those under 13"),
            RatingEntity(id = "r4", name = "R", description = "Restricted - 17 and older"),
        )

        insert(
            MovieEntity("m1", "The Transporter", "Jason Statham kicks a guy in the face", "r3"),
            MovieEntity("m2", "Transporter 2", "Jason Statham kicks a bunch of guys in the face", "r4"),
            MovieEntity("m3", "Hobbs and Shaw", "Cars, Explosions and Stuff", "r3"),
            MovieEntity("m4", "Jumanji - Welcome to the Jungle", "The Rock smolders", "r3"),
        )
        insert(
            ActorEntity("a1", "Jason Statham"),
            ActorEntity("a2", "The Rock"),
            ActorEntity("a3", "Shu Qi"),
            ActorEntity("a4", "Amber Valletta"),
            ActorEntity("a5", "Kevin Hart"),
        )
        insert(
            RoleEntity("m1", "a1", "Frank Martin", 1),
            RoleEntity("m1", "a3", "Lai", 2),
            RoleEntity("m2", "a1", "Frank Martin", 1),
            RoleEntity("m2", "a4", "Audrey Billings", 2),
            RoleEntity("m3", "a2", "Hobbs", 1),
            RoleEntity("m3", "a1", "Shaw", 2),
            RoleEntity("m4", "a2", "Spencer", 1),
            RoleEntity("m4", "a5", "Fridge", 2),
        )
    }
}