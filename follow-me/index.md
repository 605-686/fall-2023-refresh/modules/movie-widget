---
title: Movie Widget
---

We can create little widgets in our application that give the user a mini user interface on their homescreen!

In this module, we'll create a simple widget to display movie names, and when they're clicked, send the user to the display page for that movie.

Source code for examples is at
[https://gitlab.com/605-686/fall-2023-refresh/modules/movie-widget](https://gitlab.com/605-686/fall-2023-refresh/modules/movie-widget)
