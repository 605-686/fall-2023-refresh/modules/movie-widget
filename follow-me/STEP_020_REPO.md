---
title: Widget Declaration
template: main-repo.html
---

Widgets allow you to embed small parts of your application into a user's home screen.
They can display data from your application, allow the user to interact with it, and act as
customized launch points for your application.

Glance is a Jetpack Compose framework for creating and managing widgets, built on top of
the existing AppWidget framework. It looks and feels quite a bit like the Compose UIs
you've built before, but there's a really important difference: the UI is remote.

Your widget runs in a separate process from your application, *inside* the user's selected
Home application. You can't directly communicate with it, nor can you add your code to that Home
application. So how does it work?

In the AppWidget framework, you'd create *Remote Views*. They defined a limited subset of
available UI views, and acted as a proxy for your user interface. The remote views are defined
in a **Broadcast Receiver** that you define, that accepts system broadcasts describing the current
state of the widget and whether it wants an update.

Broadcast Receivers are top-level Android Components, like Activities, and are declared
in the `AndroidManifest.xml` file. When a broadcast `Intent` is sent to them, the system
creates an instance of them, passes the `Intent` to its `onReceive()` function to perform its
action, then the receiver's work is done.

You create a subclass of `AppWidgetReceiver` (or for Compose, `GlanceAppWidgetReceiver`) to
supply the remote views for the widget. When the user adds or removes a widget for their
Home app, or after an amount of time that the widget declaration states it should be updated,
a broadcast `Intent` is sent to your widget receiver. You create or update the remote views
which are sent back to the widget framework to render for the user (or remove form the Home app).

This is easier to see as we create a new widget. We'll define a widget that displays our movie
list and allows the user to click on one, taking them to that movie in the application.

## Setting up the widget

First we need to add the glance dependencies

* The {{ find("020-glance-version", "version") }} in the version catalog
* The {{ find("020-glance-libs", "libraries") }} in the version catalog
* The {{ find("020-glance-deps", "dependencies") }} in the app's build script

We'll put our code in a subpackage named `com.androidbyexample.movie.glance`. To do this, 
right-click on the `com.androidbyexample.movie` package and choose **New > package**

![Choose new package option](screenshots/widgets-new-package.png){width=400}

then add ".glance" at the end and press Enter:

![Write name of subpackage](screenshots/widgets-new-package-2.png){width=400}

!!! note

    A "subpackage" in Kotlin (or Java) is one where another package has the same
    prefix. For example, `a.b.c` is a subpackage of `a.b`.

    Aside from the name, *there is no relation between these packages*. If you `import a.b.*`,
    it doesn't make types from `a.b.c` available.

    !!! warning
    
        **Never** use import-on-demand (imports like `a.b.*`). See
        [Import on Demand is EVIL!](https://www.javadude.com/posts/20040522-import-on-demand-is-evil/)
        for details.

        !!! info

            Nested boxes are cool. So are Fezzes and Bowties.

            ![Fezzes are cool](screenshots/fez.jpg){width=300}

            (I'm in a mood today...)

Now we create some new code in that package.

The {{ find("020-create-appwidget", "`MovieAppWidget`") }} class defines your user interface
for the widget. We'll just create a `Text` for it.

!!! caution

    The composable functions for Glance, like `Text` have the same names as the ones we've
    been using, *but are in a different package*. In this case, the Glance `Text` is
    `androidx.glance.text.Text`. Be careful when adding them; be sure to choose the one from
    the Glance package.

Now the {{ find("020-create-receiver", "`MovieAppWidgetReceiver`") }}. This is the broadcast
receiver that receives the system broadcasts about the widget to add/remove/update it. All
we need to do is define its `glanceAppWidget` property.

!!! note

    We won't be doing anything fancy with widgets (and there's much more you can do). If you want
    to do more, see [Jetpack Glance](https://developer.android.com/jetpack/compose/glance) and
    [App widgets overview](https://developer.android.com/develop/ui/views/appwidgets/overview).

We need to describe to Android how our widget should behave when placed into a Home app,
including sizing information and how often to explicitly update it. This information
is specified in
{{ find("020-create-widget-info", "`app/src/main/res/xml/movie_app_widget_info.xml`") }}.

There are 
[many more options](https://developer.android.com/develop/ui/views/appwidgets#AppWidgetProviderInfo)
you can specify in the widget info file. 

We attach the widget info when 
{{ find("020-register-in-manifest", "registering our receiver") }} in 
`app/src/main/AndroidManifest.xml`. The manifest is our way of telling Android which top-level
components we've defined in our application. So far, we've only had a single Activity in there
and haven't really talked about it.

When an app is installed, Android looks at the manifest to determine what's available. The
`<intent-filter>`s specify which `Intent`s this application can handle, and which components
to send those intents to. Any application can send intents to start activities or services,
and send messages to broadcast receivers. (There are some limitations but we won't go into that
detail here).

### A little more on intents and filters

Let's take a quick diversion to explain in a bit more detail how `Intent`s and `intent-filter`s work.

When the system sees

```xml
<intent-filter>
    <action android:name="android.intent.action.MAIN" />
    <category android:name="android.intent.category.LAUNCHER" />
</intent-filter>
```

The `MAIN/LAUNCHER` combination of `action` and `category` is special - it specifies an Activity
as an entry point to the application that can be listed in the Home app to start the application.

If we had something like (untested - I haven't done this in a while...)

```xml
<intent-filter>
    <action android:name="android.intent.action.VIEW" />
    <data android:scheme="https" />
    <data android:scheme="http" />
    <category android:name="android.intent.category.BROWSABLE" />
    <category android:name="android.intent.category.DEFAULT" />
</intent-filter>
```

You'd be telling android that you can handle requests to view a URI that starts with
"http" or "https" - something like this might be used if you were creating a web
browser application. If another application sent an *implicit* intent that said "hey Android,
I want to view https://androidbyexample.com", your application would be a candidate to handle it.

If only one app is installed that has an intent filter that matches, that application is used
to perform the request. If more than one app is installed that can handle it, the user is asked
which to use.

An application can send an *explicit* intent, which indicates which application and
activity/service should handle it. We'll see this later when we talk about services.

### Using the Widget

To use the widget we need to install the application. Note that we don't need to actually run
the application from Android Studio, so if you'd prefer, you can edit the application launcher
setting by clicking the "..." next to the app launcher on the Android Studio toolbar and choosing
"Edit":

![Editing the launcher](screenshots/edit-launcher.png){width=400}

And choose "Nothing" in the *Launch* dropdown:

![Editing the launcher](screenshots/do-not-launch.png){width=400}

You don't *need* to do this, but if you don't, you'll need to return to the home screen when
the application opens so you can add the widget.

To set up the widget:

![Add and remove the widget](screenshots/add-widget.gif){width=300}

1. Run the application to install it.
2. If you didn't change the launcher to not launch the app, go to the home screen. You can do
   this by clicking the "O" icon on the emulator toolbar, or swiping up from the bottom of the
   emulator or device and quickly releasing. The gesture can be tricky to use on the emulator, so
   I recommend the toolbar.
3. Long press on an empty spot on the home screen. Note that this might be different based on
   the Home app that you're using.
4. Choose *Widgets*
5. Scroll down to the MovieWidget app
6. Long press on the widget to add to the home screen (some apps define multiple widgets)
7. Drag the widget to the homescreen position you'd like and release
8. To remove, long press on the widget and drag to "Remove". Note that this might be different based on
   the Home app that you're using. 

## All code changes
