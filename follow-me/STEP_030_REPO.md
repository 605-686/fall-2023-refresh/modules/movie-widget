---
title: Widget UI and Data
template: main-repo.html
---

Let's make this widget actually do something. We'll fetch the list of movies from the database
and display it on the widget. When the user selects a movie, they'll be taken to its display
page in the app.

Before moving forward, let's update the {{ find("030-update-widget-info", "widget metadata") }}.

* `description` adds descriptive text when choosing the widget to place on a home screen.
  This requires adding the text to strings.xml
* `minWidth` and `minHeight` limits how small the user can size the widget
* `resizeMode` specifies which directions the user can resize the widget
* `targetCellWidth` and `targetCellHeight` gives sizing when initially placing the widget. These are
   multiples of the cell size defined by the home application.
* `widgetCategory` specifies if the widget can appear on the home or lock screen (called "keyguard" here)

Widgets run in a different process than our application. The `MovieAppWidget` runs in our
application's process to create a set of *Remote Views* that are sent to the widget to be
rendered. We need to set it up to fetch data when first creating the widget and when the widget is
updated.

By default, `GlanceAppWidget` is set up to read widget state from a preference store. This is a
file that's private to your application, and is normally used to store application settings
between runs. Here it's used for the application to write widget state updates. If you only
have minor data to update, this is fine, but a movie list is typically too much data to store
in a preference store (and the data would have to be read from the database, written to
the file, then read back from the file to perform the widget update).

Instead, we'll override the update data handling.

All classes that extend `GlanceAppWidget` contain a `GlanceStateDefinition` instance that
provides a `DataStore` that the widget can use to get its data. We'll use this data store to
fetch the initial list of movies from the database. Glance gets the `data` property from the
data store, a `Flow` of our movie list in this case.

Whenever the widget is told to update (we'll see that later), Glance reads `data` again and updates
state.

Normally, if we use a `DataStore`, we read `data` once to get the `Flow` of updates, and then
collect from that `Flow` to get changes. However, the `MovieAppWidget` wants an explicit update
notification, telling it when to update its data. We'll ask for a new `Flow` each time from
our repository to get the movie list.

To do this, we need to:

* Create a {{ find("030-state-def", "state definition") }} class. This is just a factory for our data
  store. We don't need to define the `getLocation` function; it's not used (and normally
  returns a `File` indicating where the data is stored)
* Create a {{ find("030-data-store", "data store") }} that returns the actual data. Its data property
  returns a new `Flow<List<MovieDto>>` each time it's read.
* Add a {{ find("030-stateDefinition-prop", "state definition property") }} in our `MovieAppWidget`
  so it knows how to create its data store (and then get its data). Because the widget doesn't
  update the data, we don't implement the `updateData` function.
* {{ find("030-get-state", "Get the current state") }} so we can display it.

We need to explicitly request widget updates when the UI changes data. This is done via

```kotlin
MovieAppWidget().updateAll(context)
```

!!! note

    This will update all instances of the movie widget that the user has placed on their home
    screens. It's possible to update just a specific instance if you'd like, but we won't go into
    that detail here. (Think as an example, different instances of a picture-frame widget
    that host different images - you click on one to take you to the app to select an image,
    and it updates only that instance of the widget)

Ideally, we want to make this update call in as few places as possible, while ensuring we cover
all cases. In our application, this is simple - we can 
{{ find("030-watch-changes", "watch the movie flow") }} for emissions and call update. For this
purpose, we start a coroutine when the view model is initialized and collect the movie flow.

The `updateAll()` call requires a context. We can access the application context (which lives
across the lifetime of the application, vs an activity context that only lives while the
activity is running) by {{ find("030-change-to-androidviewmodel", "changing our view model") }}
to subclass `AndroidViewModel` and passing in an `Application`. `AndroidViewModel` contains a
`getApplication()` function that allows access when we call `updateAll()`.

This requires that we {{ find("030-pass-application", "pass the application") }} when creating
our view model in its factory.

The {{ find("030-ui", "UI") }} for our `MovieAppWidget` uses a `LazyColumn` to display our list of
movies. When a movie title is {{ find("030-click", "clicked") }}, we start our `MainActivity` and
pass the movie id as a parameter.

Note that `actionStartActivity` creates a lambda for us, so we pass it in parentheses to `clickable`
rather than put it in a lambda. If we had instead written

```kotlin
.clickable {
    actionStartActivity<MainActivity>(
        actionParametersOf(movieIdKey to movie.id)
    )
}
```

we'd be telling it to call `actionStartActivity` to *create* the lambda when the button is clicked.
We need to create that lambda and use it as the function called when the button is clicked.

The created lambda creates an explicit `Intent` targeting our activity, and uses the 
`actionParametersOf` function to add key and value pairs to the `extras` map of that intent.

The name specified in the {{ find("030-key", "`movieIdKey`") }} must match the name we use
to retrieve the data from the intent in `MainActivity`. We define the
{{ find("030-key-name", "key name") }} as a constant in `MainActivity` to ensure the name is
consistent.

The fun part is now dealing with that intent coming into the activity. The activity may or may not
already be running. If you were running the application and went to the home screen, the system
may keep the activity instance around until it needs to reclaim its memory. However, it could kill
it at any time when it's not the current foreground application. This leads to two ways we must deal
with the intent:

* if the activity is *not* running, a new instance will be created and the intent passed to `onCreate`
* if the activity *is* running, the intent will be passed to the `onNewIntent()` function of the existing activity instance

We need to handle the intent in both places, so we create a
{{ find("030-handle-movie-id", "helper function") }} that tries to pull the movie id out of an
intent. If it's not present, we reset the stack to be just `MovieList`. If it is present, we set
the stack to be `MovieList` followed by `MovieDisplay` for that movie. This presents the movie to
the user and allows them to press back to get to the movie list.

We call `handleMovieId()` from
{{ find("030-on-create", "`onCreate`") }} and
{{ find("030-on-new-intent", "`onNewIntent`") }}.
Note that `onNewIntent` is passed an `Intent` as a parameter, while `onCreate` uses the property
`intent` defined by the activity as the `Intent` that launched it.

If we run the application now, we'll get some interesting results if we

1. Click on a movie in the widget (and see the movie displayed)
2. Go back to home
3. Click on a different movie in the widget (and see it displayed)
4. Press back repeatedly

![Bad stack!](screenshots/need-launch-mode.gif){width=300}

The problem is that each time we start the activity from the widget, a new instance is pushed
on the stack for the application process. We now have a stack of activities, managed by the
system, each of which contains a stack of screens. Originally, Android applications used an
`Actitity` for each screen, calling `startActvitity` to push new instances on the system-managed
stacks. Over time we evolved to using a single `Activity` approach.

To fix this, we set the {{ find("030-launch-mode", "launch mode") }} for our `MainActivity`
to `singleInstance`, which ensures we only ever have one instance of the activity.

## All code changes
